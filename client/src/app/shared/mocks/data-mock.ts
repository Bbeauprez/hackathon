import { Projet } from 'src/app/dashboard/projets/projet.model';
import { User } from '../models/user.model';

export const PROJETS: Projet[] = [
  {id: 1, name: 'projet 1', client: 'ESGI', dueDate: '2020-05-12', deliveratedDate: '28-05-2020', dev: 'tata'},
  {id: 2, name: 'projet 2', client: 'ESGI', dueDate: '2020-06-21', deliveratedDate: '30-06-2020', dev: 'tete'},
  {id: 3, name: 'projet 3', client: 'ESGI', dueDate: '2020-07-10', deliveratedDate: null, dev: 'titi'},
  {id: 4, name: 'projet 4', client: 'ESGI', dueDate: '2020-07-30', deliveratedDate: null, dev: 'tutu'}
];
export const USERS: User[] = [
  {
    id: 1, email: 'basile@test.fr', firstname: 'basile', lastname: 'beauprez', isValidated: true, isDeleted: false,
    siren: 'siren'
  },
  {
    id: 2, email: 'toto@test.fr', firstname: 'toto', lastname: 'toto', isValidated: true, isDeleted: false,
    siren: 'siren'
  }, {
    id: 3, email: 'titi@test.fr', firstname: 'titi', lastname: 'titi', isValidated: true, isDeleted: false,
    siren: 'siren'
  }, {
    id: 4, email: 'tutu@test.fr', firstname: 'tutu', lastname: 'tutu', isValidated: false, isDeleted: false,
    siren: ''
  }];
