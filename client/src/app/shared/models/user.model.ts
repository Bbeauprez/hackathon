export interface User {
    id: number;
    email: string;
    password?: string;
    firstname: string;
    lastname: string;
    isValidated: boolean;
    isDeleted: boolean;
    siren: string;
    token?: string;
}
