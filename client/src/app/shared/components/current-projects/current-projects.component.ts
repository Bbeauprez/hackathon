import { Component, OnInit } from '@angular/core';
import { PROJETS } from '../../mocks/data-mock';
import { Projet } from 'src/app/dashboard/projets/projet.model';

@Component({
  selector: 'app-current-projects',
  templateUrl: './current-projects.component.html',
  styleUrls: ['./current-projects.component.scss']
})
export class CurrentProjectsComponent implements OnInit {
  projets: Projet[] = PROJETS;
  constructor() { }

  ngOnInit() {
    this.projets = this.projets.filter(projet => projet.deliveratedDate === ('' || null));
    console
  }

}
