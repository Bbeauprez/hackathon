import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GreetingsBubbleComponent } from './greetings-bubble.component';

describe('GreetingsBubbleComponent', () => {
  let component: GreetingsBubbleComponent;
  let fixture: ComponentFixture<GreetingsBubbleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GreetingsBubbleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GreetingsBubbleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
