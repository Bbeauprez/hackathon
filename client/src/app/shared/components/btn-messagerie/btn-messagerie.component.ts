import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { faEnvelopeSquare } from '@fortawesome/free-solid-svg-icons'
import { BtnMessagerieService } from './btn-messagerie.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-btn-messagerie',
  templateUrl: './btn-messagerie.component.html',
  styleUrls: ['./btn-messagerie.component.scss']
})
export class BtnMessagerieComponent implements OnInit, OnDestroy {
  isOpen: boolean = false;
  faEnvelopeSquare = faEnvelopeSquare;
  subscription: Subscription;
  
  constructor(private btnMessagerieService: BtnMessagerieService) {
    this.subscription = this.btnMessagerieService.messagerieOpen$.subscribe( value => {
      this.isOpen = value;
    })
  }

  ngOnInit() {
  }

  ngOnDestroy(){
    this.subscription.unsubscribe();
  }

  openMessagerie(event) {
    if (!this.isOpen) {
      this.btnMessagerieService.displayMessagerie(event);
      this.isOpen = true;
    } else {
      this.btnMessagerieService.displayMessagerie(false);
      this.isOpen = false;
    }
  }

}
