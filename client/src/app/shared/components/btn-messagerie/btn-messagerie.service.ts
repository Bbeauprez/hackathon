import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BtnMessagerieService {
  private messagerieOpenSource: Subject<boolean> = new Subject<boolean>();  
  messagerieOpen$ = this.messagerieOpenSource.asObservable();

  constructor() { }

  displayMessagerie(event: boolean){
    console.log(event);
    this.messagerieOpenSource.next(event);
  }
}
