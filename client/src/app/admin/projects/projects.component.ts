import {ChangeDetectorRef, Component, Inject, OnInit} from '@angular/core';
import {Projet} from '../../dashboard/projets/projet.model';
import {PROJETS} from 'src/app/shared/mocks/data-mock';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {MatTableDataSource} from '@angular/material/table';

export interface DialogData {
  name: string;
  clientName: string;
  dueDate: any;
  Docs: any;
}

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.scss']
})
export class ProjectsComponent implements OnInit {
  projects: Projet[] = PROJETS;
  displayedColumns: string[] = ['name', 'dueDate', 'dev', 'docs', 'status'];
  dataSource: MatTableDataSource<any>;

  constructor(public dialog: MatDialog, private changeDetectorRefs: ChangeDetectorRef) {
  }

  name: string;
  clientName: string;
  dueDate: any;
  docs: any;

  ngOnInit() {
    this.dataSource = new MatTableDataSource(this.projects);
  }

  openDialog(data, idx): void {
    if (data !== null && data !== undefined) {
      this.name = data.name;
      this.clientName = data.clientName;
      this.dueDate = data.dueDate;
      this.docs = data.docs;
    }
    const dialogRef = this.dialog.open(AddProjectComponent, {
      width: '250px',
      data: {name: this.name, clientName: this.clientName, dueDate: this.dueDate, docs: this.docs}
    });

    dialogRef.afterClosed().subscribe(result => {
        if (data !== null && data !== undefined) {
          console.log(data);
          this.projects[idx] = {
            client: result.clientName,
            id: this.projects.length,
            name: result.name,
            dueDate: result.dueDate ? result.dueDate : data.dueDate,
            deliveratedDate: data.dueDate ? data.dueDate : null,
            dev: data.dev
          };
        } else {
          this.projects.push({
            client: result.clientName,
            id: this.projects.length,
            name: result.name,
            dueDate: result.dueDate,
            deliveratedDate: null,
            dev: null
          });
        }
        this.dataSource = new MatTableDataSource(this.projects);
      }
    );
  }

}

@Component({
  selector: 'app-add-project',
  templateUrl: 'addProjectDialog.html',
})
export class AddProjectComponent {

  constructor(
    public dialogRef: MatDialogRef<AddProjectComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
