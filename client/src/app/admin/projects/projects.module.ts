import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AddProjectComponent, ProjectsComponent} from './projects.component';
import {MatTableModule} from '@angular/material/table';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatDialogModule} from '@angular/material/dialog';
import {MatButtonModule} from '@angular/material/button';
import {FormsModule} from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';



@NgModule({
  declarations: [ProjectsComponent, AddProjectComponent],
  imports: [
    CommonModule,
    MatTableModule,
    MatFormFieldModule,
    MatDialogModule,
    MatButtonModule,
    FormsModule,
    MatInputModule,
    MatSelectModule
  ],
  entryComponents: [
    AddProjectComponent
  ]
})
export class ProjectsModule { }
