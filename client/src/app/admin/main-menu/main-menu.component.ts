import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/shared/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-main-menu',
  templateUrl: './main-menu.component.html',
  styleUrls: ['./main-menu.component.scss']
})
export class MainMenuComponent implements OnInit {

  constructor(
      private authService: AuthService,
      private router: Router
    ) { }

  ngOnInit() {
  }

  logout(){
    console.log("aze");
    this.authService.logout();
    this.router.navigate(['/login']);
  }

}
