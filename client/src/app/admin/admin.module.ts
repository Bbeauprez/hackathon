import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin.component';
import { ProjectsComponent } from './projects/projects.component';
import {ProjectsModule} from './projects/projects.module';
import { UserComponent } from './user/user.component';
import {UserModule} from './user/user.module';


@NgModule({
  declarations: [AdminComponent],
  imports: [
    CommonModule,
    AdminRoutingModule,
    ProjectsModule,
    UserModule
  ]
})
export class AdminModule { }
