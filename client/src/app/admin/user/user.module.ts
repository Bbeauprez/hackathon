import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AddUserComponent, UserComponent} from './user.component';
import {AddProjectComponent} from '../projects/projects.component';
import {MatTableModule} from '@angular/material/table';



@NgModule({
  declarations: [UserComponent, AddUserComponent],
  imports: [
    CommonModule,
    MatTableModule
  ],
  entryComponents: [
    AddUserComponent
  ]
})
export class UserModule { }
