import {Component, Inject, OnInit} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {PROJETS, USERS} from '../../shared/mocks/data-mock';
import {User} from '../../shared/models/user.model';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {DialogData} from '../projects/projects.component';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  displayedColumns: string[] = ['name', 'surname', 'mail', 'siren', 'deactivate'];
  dataSource: MatTableDataSource<any>;
  users: User[] = USERS;
  email: any;
  firstname: any;
  id: any;
  isDeleted: any;
  isValidated: any;
  lastname: any;
  siren: any;

  constructor(public dialog: MatDialog) {
  }

  ngOnInit() {
    this.dataSource = new MatTableDataSource(this.users);
  }

  openDialog(data, idx): void {
    if (data !== null && data !== undefined) {

    }
    const dialogRef = this.dialog.open(AddUserComponent, {
      width: '250px',
      data: {
        email: this.email,
        firstname: this.firstname,
        id: this.id,
        isDeleted: this.isDeleted,
        isValidated: this.isValidated,
        lastname: this.lastname,
        siren: this.siren
      }
    });

    dialogRef.afterClosed().subscribe(result => {
        if (data !== null && data !== undefined) {
          this.users[idx] = {
            email: result.mail,
            firstname: result.name,
            id: this.users.length,
            isDeleted: false,
            isValidated: result.validated,
            lastname: result.lastName,
            siren: result.siren
          };
        } else {
          this.users.push({
            email: result.mail,
            firstname: result.name,
            id: this.users.length,
            isDeleted: false,
            isValidated: result.validated,
            lastname: result.lastName,
            siren: result.siren
          });
        }
        this.dataSource = new MatTableDataSource(this.users);
      }
    );
  }

}

@Component({
  selector: 'app-add-project',
  templateUrl: 'addUserDialog.html',
})
export class AddUserComponent {

  constructor(
    public dialogRef: MatDialogRef<AddUserComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
