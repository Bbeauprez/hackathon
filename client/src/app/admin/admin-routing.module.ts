import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {AdminComponent} from './admin.component';
import {MainMenuComponent} from './main-menu/main-menu.component';
import {ProjectsComponent} from './projects/projects.component';
import {UserComponent} from './user/user.component';

const routes: Routes = [
  {
    path: '',
    component: AdminComponent,
    children: [
      {
        path: 'projects',
        component: ProjectsComponent
      },
      {
        path: 'user',
        component: UserComponent
      },
    ]
  }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  declarations: [
    MainMenuComponent
  ],
  exports: [RouterModule, MainMenuComponent]
})
export class AdminRoutingModule {
}
