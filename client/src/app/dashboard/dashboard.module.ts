import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { MainMenuComponent } from './main-menu/main-menu.component';
import { BtnMessagerieComponent } from '../shared/components/btn-messagerie/btn-messagerie.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ChatBotComponent } from './chat-bot/chat-bot.component'
import { CurrentProjectsComponent } from '../shared/components/current-projects/current-projects.component';
import { MessagerieComponent } from './messagerie/messagerie.component';
import { GreetingsBubbleComponent } from '../shared/components/greetings-bubble/greetings-bubble.component';
import { BadgeAdwComponent } from '../shared/components/badge-adw/badge-adw.component';



@NgModule({
  declarations: [
    DashboardComponent,
    MainMenuComponent,
    BtnMessagerieComponent,
    ChatBotComponent,
    CurrentProjectsComponent,
    MessagerieComponent,
    GreetingsBubbleComponent,
    BadgeAdwComponent,
  ],
  imports: [
    CommonModule,
    FontAwesomeModule,
    DashboardRoutingModule,
  ]
})
export class DashboardModule { }
