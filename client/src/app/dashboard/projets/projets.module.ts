import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProjetsRoutingModule } from './projets-routing.module'
import { ProjetsComponent } from './projets.component';
import { ProjetDetailComponent } from './projet-detail/projet-detail.component';
import { ProjetListComponent } from './projet-list/projet-list.component';
import { MatTableModule } from '@angular/material/table'




@NgModule({
  declarations: [ProjetsComponent, ProjetDetailComponent, ProjetListComponent],
  imports: [
    CommonModule,
    MatTableModule,
    ProjetsRoutingModule,
  ]
})
export class ProjetsModule { }
