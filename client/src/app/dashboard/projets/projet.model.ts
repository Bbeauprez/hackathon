export interface Projet {
    id: number;
    name: string;
    client: string;
    dueDate: any;
    deliveratedDate: any;
    dev: any;
    gitLink?: string;
}
