import { Component, OnInit } from '@angular/core';
import { PROJETS } from '../../../shared/mocks/data-mock';
import { ProjetsService } from '../projets.service';
import { map } from 'rxjs/operators';
import { Projet } from 'src/app/dashboard/projets/projet.model';


@Component({
  selector: 'app-projet-list',
  templateUrl: './projet-list.component.html',
  styleUrls: ['./projet-list.component.scss']
})
export class ProjetListComponent implements OnInit {
  projets: Projet[] = PROJETS;
  // projets: any;
  displayedColumns: string[] = ['name', 'dueDate']

  constructor(private projetsService: ProjetsService) { }

  ngOnInit() {
    // this.projetsService.getProjets().subscribe( projets => {
    //   this.projets = { ...projets };
    //   console.log(this.projets);
    // });
  }

}
