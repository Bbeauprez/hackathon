import { Component, OnInit, OnDestroy } from '@angular/core';
import { BtnMessagerieService } from 'src/app/shared/components/btn-messagerie/btn-messagerie.service';
import { Subscription } from 'rxjs';
import { Message } from './messagerie.model';
import { MESSAGES } from './messagerie-mock';

declare var $: any;

@Component({
  selector: 'app-messagerie',
  templateUrl: './messagerie.component.html',
  styleUrls: ['./messagerie.component.scss']
})
export class MessagerieComponent implements OnInit, OnDestroy {
  subscription: Subscription;
  isOpen: boolean;
  messages: Message[] = MESSAGES;

  constructor(private btnMessagerieService: BtnMessagerieService) {
    this.subscription = btnMessagerieService.messagerieOpen$.subscribe( value => {
      this.isOpen = value;
    })
  }

  ngOnInit() {

  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  messageOnClick(message) {
    const truc = this.messages.filter(message => message.active);
    console.log(truc);
    message.active = !message.active;
  }

  closeMessagerie(){
    this.btnMessagerieService.displayMessagerie(false);
  }
}
