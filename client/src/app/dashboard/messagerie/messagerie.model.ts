export interface Message {
  id: number;
  subject: string;
  content: string
  sendAt: string;
  isRead: boolean;
  isDeleted: boolean;
  active?: boolean;
}