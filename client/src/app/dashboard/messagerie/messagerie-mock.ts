import { Message } from './messagerie.model';

export const MESSAGES: Message[] = [
  {id: 1, subject: 'Un objet de mail', content: 'un contenu de mail ultra long', sendAt: '05-03-2020', isRead: false, isDeleted: false},
  {id: 2, subject: 'Un objet de mail', content: 'un contenu de mail ultra long', sendAt: '05-03-2020', isRead: false, isDeleted: false},
  {id: 3, subject: 'Un objet de mail', content: 'un contenu de mail ultra long', sendAt: '05-03-2020', isRead: false, isDeleted: false},
  {id: 4, subject: 'Un objet de mail', content: 'un contenu de mail ultra long', sendAt: '05-03-2020', isRead: false, isDeleted: false},
];