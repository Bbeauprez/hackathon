import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import { PartnerGuard } from './partner.guard';
import { CurrentProjectsComponent } from '../shared/components/current-projects/current-projects.component';


const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    canActivate: [PartnerGuard],
    children: [
      {
        path: '',
        component: CurrentProjectsComponent
      },
      {
        path: 'projets',
        loadChildren: () => import ('./projets/projets.module').then(mod => mod.ProjetsModule),
      },
      {
        path: 'planning',
        loadChildren: () => import ('./planning/planning.module').then(mod => mod.PlanningModule),
      }

  ]},
  { path: 'planning', loadChildren: () => import('./planning/planning.module').then(m => m.PlanningModule) },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
