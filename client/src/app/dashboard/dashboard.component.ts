import { Component, OnInit, HostListener } from '@angular/core';
import { BtnMessagerieService } from '../shared/components/btn-messagerie/btn-messagerie.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  subscription: Subscription;
  isOpen: boolean;

  constructor(private btnMessagerieService: BtnMessagerieService) {
    this.subscription = this.btnMessagerieService.messagerieOpen$.subscribe( value => {
      console.log('subscribe dashboard', value);
      this.isOpen = value;
    })
  }

  ngOnInit() {
  }


}
