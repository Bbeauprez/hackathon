<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 * attributes={"security"="is_granted('ROLE_USER')"},
 *     collectionOperations={
 *         "get",
 *         "post"={"security"="is_granted('ROLE_ADMIN')"}
 *     },
 *     itemOperations={
 *         "get",
 *         "delete"={"security"="is_granted('ROLE_ADMIN')"},
 *        "put"={"security"="is_granted('ROLE_ADMIN')"},
 *         "patch"={"security"="is_granted('ROLE_ADMIN')"},
 *     },
 *     normalizationContext={"groups"={"output"}},
 *     denormalizationContext={"groups"={"input"}}
 * )
 * @ORM\Entity(repositoryClass="App\Repository\MessageRepository")
 */
class Message
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @Groups({"output", "input"})
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Groups({"output", "input"})
     * @ORM\Column(type="string", length=100)
     */
    private $subject;

    /**
     * @Groups({"output", "input"})
     * @ORM\Column(type="string", length=255)
     */
    private $content;

    /**
     * @Groups({"output", "input"})
     * @ORM\Column(type="datetime")
     */
    private $sendAt;

    /**
     * @Groups({"output", "input"})
     * @ORM\Column(type="boolean")
     */
    private $isRead;

    /**
     * @Groups({"output", "input"})
     * @ORM\Column(type="boolean")
     */
    private $isDeleted;

    /**
     * @Groups({"output", "input"})
     * @ORM\ManyToOne(targetEntity="Users", inversedBy="messages")
     */
    private $targetUser;

    /**
     * @Groups({"output", "input"})
     * @ORM\ManyToOne(targetEntity="Users", inversedBy="messagesSend")
     */
    private $author;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSubject(): ?string
    {
        return $this->subject;
    }

    public function setSubject(string $subject): self
    {
        $this->subject = $subject;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getSendAt(): ?\DateTimeInterface
    {
        return $this->sendAt;
    }

    public function setSendAt(\DateTimeInterface $sendAt): self
    {
        $this->sendAt = $sendAt;

        return $this;
    }

    public function getIsDeleted(): ?bool
    {
        return $this->isDeleted;
    }

    public function setIsDeleted(bool $isDeleted): self
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }

    public function getTargetUser()
    {
        return $this->targetUser;
    }

    public function setTargetUser($targetUser): void
    {
        $this->targetUser = $targetUser;
    }

    public function getIsRead()
    {
        return $this->isRead;
    }

    public function setIsRead($isRead): void
    {
        $this->isRead = $isRead;
    }

    public function getAuthor()
    {
        return $this->author;
    }

    public function setAuthor($author): void
    {
        $this->author = $author;
    }


}
