<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 * attributes={"security"="is_granted('ROLE_USER')"},
 *     collectionOperations={
 *         "get",
 *         "post"={"security"="is_granted('ROLE_ADMIN')"}
 *     },
 *     itemOperations={
 *         "get",
 *         "delete"={"security"="is_granted('ROLE_ADMIN')"},
 *         "put"={"security"="is_granted('ROLE_ADMIN')"},
 *         "patch"={"security"="is_granted('ROLE_ADMIN')"},
 *     },
 *     normalizationContext={"groups"={"output"}},
 *     denormalizationContext={"groups"={"input"}}
 * )
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class Users implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"input","output"})
     */
    private $id;

    /**
     * @Groups({"input"})
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @Groups({"output"})
     * @ORM\Column(type="string", length=255)
     */
    private $firstName;

    /**
     * @Groups({"output", "input"})
     * @ORM\Column(type="string", length=255)
     */
    private $lastName;

    /**
     * @Groups({"output", "input"})
     * @ORM\Column(type="string", length=255)
     */
    private $address;

    /**
     * @Groups({"output", "input"})
     * @ORM\Column(type="string", length=255)
     */
    private $mail;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $password;

    /**
     * @Groups({"output", "input"})
     * @ORM\Column(type="string", length=255)
     */
    private $siren;

    /**
     * @Groups({"output", "input"})
     * @ORM\Column(type="boolean")
     */
    private $isValidated;

    /**
     * @Groups({"output", "input"})
     * @ORM\Column(type="boolean")
     */
    private $isDeleted;

    /**
     * @Groups({"output", "input"})
     * @ORM\ManyToMany(targetEntity="Tags")
     */
    private $tags;

    /**
     * @Groups({"output", "input"})
     * @ORM\ManyToMany(targetEntity="Newsletter")
     */
    private $eventInterested;
    /**
     * @Groups({"output", "input"})
     * @ORM\OneToMany(targetEntity="Project", mappedBy="developer")
     */
    private $projects;

    /**
     * @Groups({"output", "input"})
     * @ORM\OneToMany(targetEntity="Message", mappedBy="targetUser")
     */
    private $messages;

    /**
     * @Groups({"output", "input"})
     * @ORM\OneToMany(targetEntity="Message", mappedBy="author")
     */
    private $messagesSend;

    /**
     * @Groups({"output", "input"})
     * @ORM\OneToMany(targetEntity="Newsletter", mappedBy="author")
     */
    private $writtenNews;

    /**
     * @Groups({"output", "input"})
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $isActive;

    /**
     * @Groups({"input"})
     * @var string plainPassword
     */
    private $plainPassword;

    public function __construct($mail)
    {
        $this->isActive = true;
        $this->mail = $mail;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getMail(): ?string
    {
        return $this->mail;
    }

    public function setMail(string $mail): self
    {
        $this->mail = $mail;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getIsValidated(): ?bool
    {
        return $this->isValidated;
    }

    public function setIsValidated(bool $isValidated): self
    {
        $this->isValidated = $isValidated;

        return $this;
    }

    public function getIsDeleted(): ?bool
    {
        return $this->isDeleted;
    }

    public function setIsDeleted(bool $isDeleted): self
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }

    public function getTags()
    {
        return $this->tags;
    }

    public function setTags($tags): void
    {
        $this->tags = $tags;
    }

    public function getProjects()
    {
        return $this->projects;
    }

    public function setProjects($project): void
    {
        $this->projects = $project;
    }

    public function getMessages()
    {
        return $this->messages;
    }

    public function setMessages($messages): void
    {
        $this->messages = $messages;
    }

    public function getMessagesSend()
    {
        return $this->messagesSend;
    }

    public function setMessagesSend($messagesSend): void
    {
        $this->messagesSend = $messagesSend;
    }

    public function getSiren()
    {
        return $this->siren;
    }

    public function setSiren($siren): void
    {
        $this->siren = $siren;
    }

    public function getEventInterested()
    {
        return $this->eventInterested;
    }

    public function setEventInterested($eventInterested): void
    {
        $this->eventInterested = $eventInterested;
    }

    public function getWrittenNews()
    {
        return $this->writtenNews;
    }

    public function setWrittenNews($writtenNews): void
    {
        $this->writtenNews = $writtenNews;
    }

    public function getRoles(): array
    {
        return $this->roles;
    }

    public function setRoles(array $roles): void
    {
        $this->roles = $roles;
    }

    public function getSalt()
    {
        return null;
    }

    function getUsername()
    {
        return $this->getMail();
    }

    public function eraseCredentials()
    {
        $this->plainPassword = null;
    }

    public function isActive(): bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): void
    {
        $this->isActive = $isActive;
    }

    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    public function setPlainPassword(string $plainPassword)
    {
        $this->plainPassword = $plainPassword;
    }


}
