<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 * attributes={"security"="is_granted('ROLE_USER')"},
 *     collectionOperations={
 *         "get",
 *         "post"={"security"="is_granted('ROLE_ADMIN')"}
 *     },
 *     itemOperations={
 *         "get",
 *         "delete"={"security"="is_granted('ROLE_ADMIN')"},
 *         "put"={"security"="is_granted('ROLE_ADMIN')"},
 *         "patch"={"security"="is_granted('ROLE_ADMIN')"},
 *     },
 *     normalizationContext={"groups"={"output"}},
 *     denormalizationContext={"groups"={"input"}}
 * )
 * @ORM\Entity(repositoryClass="App\Repository\ProjectRepository")
 */
class Project
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @Groups({"output", "input"})
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Groups({"output", "input"})
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @Groups({"output", "input"})
     * @ORM\Column(type="date")
     */
    private $dueDate;

    /**
     * @Groups({"output", "input"})
     * @ORM\Column(type="datetime")
     */
    private $deliveratedDate;

    /**
     * @Groups({"output", "input"})
     * @ORM\Column(type="string", length=255)
     */
    private $linkRepo;

    /**
     * @Groups({"output", "input"})
     * @ORM\ManyToOne(targetEntity="Users", inversedBy="projects")
     */
    private $developer;

    /**
     * @Groups({"output", "input"})
     * @ORM\ManyToMany(targetEntity="Tags")
     */
    private $tags;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDueDate(): ?\DateTimeInterface
    {
        return $this->dueDate;
    }

    public function setDueDate(\DateTimeInterface $dueDate): self
    {
        $this->dueDate = $dueDate;

        return $this;
    }

    public function getDeliveratedDate(): ?\DateTimeInterface
    {
        return $this->deliveratedDate;
    }

    public function setDeliveratedDate(\DateTimeInterface $deliveratedDate): self
    {
        $this->deliveratedDate = $deliveratedDate;

        return $this;
    }

    public function getLinkRepo(): ?string
    {
        return $this->linkRepo;
    }

    public function setLinkRepo(string $linkRepo): self
    {
        $this->linkRepo = $linkRepo;

        return $this;
    }

    public function getDeveloper()
    {
        return $this->developer;
    }

    public function setDeveloper($developer): void
    {
        $this->developer = $developer;
    }

    public function getTags()
    {
        return $this->tags;
    }

    public function setTags($tags): void
    {
        $this->tags = $tags;
    }

}
