<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     attributes={"security"="is_granted('ROLE_USER')"},
 *     collectionOperations={
 *         "get",
 *         "post"={"security"="is_granted('ROLE_ADMIN')"}
 *     },
 *     itemOperations={
 *         "get",
 *         "delete"={"security"="is_granted('ROLE_ADMIN')"},
 *         "put"={"security"="is_granted('ROLE_ADMIN')"},
 *         "patch"={"security"="is_granted('ROLE_ADMIN')"},
 *     },
 *     normalizationContext={"groups"={"output"}},
 *     denormalizationContext={"groups"={"input"}}
 * )
 * @ORM\Entity(repositoryClass="App\Repository\NewsletterRepository")
 */
class Newsletter
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @Groups({"output", "input"})
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Groups({"output", "input"})
     * @ORM\Column(type="string", length=255)
     */
    private $tittle;

    /**
     * @Groups({"output", "input"})
     * @ORM\Column(type="string", length=255)
     */
    private $description;

    /**
     * @Groups({"output"})
     * @ORM\ManyToMany(targetEntity="Users")
     */
    private $userInterested = [];

    /**
     * @Groups({"output", "input"})
     * @ORM\Column(type="datetime")
     */
    private $postedAt;

    /**
     * @Groups({"output"})
     * @ORM\ManyToOne(targetEntity="Users", inversedBy="writtenNews"))
     */
    private $author;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTittle(): ?string
    {
        return $this->tittle;
    }

    public function setTittle(string $tittle): self
    {
        $this->tittle = $tittle;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getUserInterested(): ?array
    {
        return $this->userInterested;
    }

    public function setUserInterested(array $userInterested): self
    {
        $this->userInterested = $userInterested;

        return $this;
    }

    public function getPostedAt(): ?\DateTimeInterface
    {
        return $this->postedAt;
    }

    public function setPostedAt(\DateTimeInterface $postedAt): self
    {
        $this->postedAt = $postedAt;

        return $this;
    }

    public function getAuthor(): ?string
    {
        return $this->author;
    }

    public function setAuthor(string $author): self
    {
        $this->author = $author;

        return $this;
    }
}
